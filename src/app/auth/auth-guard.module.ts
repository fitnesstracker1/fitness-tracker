import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad} from '@angular/router';
import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import { Route } from '@angular/compiler/src/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../app.reducer';
import { take } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate,CanLoad{
    constructor(private authServ:AuthService,private route:Router,
        private store:Store<fromRoot.State>){

    }
    //if we dont use lazing loading use canActivate otherwise not needed.Just for Reference
    //it was there
    canActivate(route:ActivatedRouteSnapshot,state:RouterStateSnapshot){
      return this.store.select(fromRoot.getIsAuthenticated).pipe(take(1)) 
      //why using pipe is this store will return a value whenever state is changing so we 
      //just want to take 1 value for that using take operator.
    }
    canLoad(route:Route){
        return this.store.select(fromRoot.getIsAuthenticated).pipe(take(1))
    }
}