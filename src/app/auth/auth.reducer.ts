import { Action } from '@ngrx/store';
import *as AuthAction from './auth.action'

export interface State {
    isAuthenticated:boolean
}

const initState:State={
    isAuthenticated:false
}

export function authReducer(state=initState,action:AuthAction.AuthActions){
    switch(action.type){
        case AuthAction.SET_AUTHENTICATED:
            return {
                isAuthenticated:true
            };
        case AuthAction.SET_UNAUTHENTICATED:
            return {
                isAuthenticated:false
            }
        default:
            return state;
    }
}

export const getIsAuthenticated =(state:State)=>state.isAuthenticated;