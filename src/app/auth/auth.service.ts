import { User } from "./user.model";
import { AuthData } from "./auth-data.model";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { TrainingService } from '../training/training.service';
import { UIservice } from '../shared/ui.service';
import { Store } from '@ngrx/store';
import * as fromRoot from '../app.reducer';
import * as UI_actions from '../shared/ui.action';
import * as AuthActions from './auth.action'

@Injectable()
export class AuthService{
    private user:User;

    constructor(private route:Router,private angularFireAuth:AngularFireAuth,
        private trainingServ:TrainingService,private uiServ:UIservice,
        private store:Store<fromRoot.State>){}

    onRegister(authData:AuthData){
        //this.uiServ.isLoadingStateChanged.next(true);
        this.store.dispatch(new UI_actions.StartLoading());
        this.angularFireAuth.auth.createUserWithEmailAndPassword(authData.email,authData.password)
        .then(result=>{
            //this.uiServ.isLoadingStateChanged.next(false)
            this.store.dispatch(new UI_actions.StopLoading());
        }).catch(error=>{
            //this.uiServ.isLoadingStateChanged.next(false)
            this.store.dispatch(new UI_actions.StopLoading());
            this.uiServ.showSnackBar(error.message,null,3000);
            console.log(error)
        })
       
    }
    onLogin(authData:AuthData){
        //this.uiServ.isLoadingStateChanged.next(true);
        this.store.dispatch(new UI_actions.StartLoading());
        this.angularFireAuth.auth.signInWithEmailAndPassword(authData.email,authData.password)
        .then(result=>{
            //this.uiServ.isLoadingStateChanged.next(false)
            this.store.dispatch(new UI_actions.StopLoading());
        }).catch(error=>{
            //this.uiServ.isLoadingStateChanged.next(false)
            this.store.dispatch(new UI_actions.StopLoading())
            this.uiServ.showSnackBar(error.message,null,3000);
            console.log(error)
        })
    }
    initAuthListener(){
        this.angularFireAuth.authState.subscribe(user=>{
            if(user){
                this.store.dispatch(new AuthActions.SetAuthenticated())
                this.route.navigate(['/training'])
            }
            else{
                this.store.dispatch(new AuthActions.SetUnathenticated())
                this.trainingServ.cancelFirebaseSubs()       
                this.route.navigate(['/login'])
            }
        })
    }
    logOut(){
        this.angularFireAuth.auth.signOut();
    }
}