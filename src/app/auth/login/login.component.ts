import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { UIservice } from 'src/app/shared/ui.service';
import { Subscription, Observable } from 'rxjs';
import { Store} from '@ngrx/store';
import * as fromRoot from '../../app.reducer'; 
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  loginForm:FormGroup
  isLoading$:Observable<boolean>;
  LoadingSub:Subscription
  constructor(private authService:AuthService,private uiServ:UIservice,
    private store:Store<fromRoot.State>) { }

  ngOnInit(): void {
   this.isLoading$=this.store.select(fromRoot.getIsLoading);
    // this.LoadingSub=this.uiServ.isLoadingStateChanged.subscribe(isLoadingState=>{
    //   this.isLoading=isLoadingState;
    // })
    this.loginForm=new FormGroup({
      email:new FormControl('',
        {validators:[Validators.required,Validators.email]}),
      password:new FormControl('',{
        validators:[Validators.required]
      })
    })
  }
  onSubmit(){
    console.log(this.loginForm)
    this.authService.onLogin({
      email:this.loginForm.value.email,
      password:this.loginForm.value.password
    })
  }
  // ngOnDestroy(){
  //   if(this.LoadingSub){
  //   this.LoadingSub.unsubscribe()
  //   }
  // }
}
