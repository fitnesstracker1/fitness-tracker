import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { UIservice } from 'src/app/shared/ui.service';
import { Subscription, Observable } from 'rxjs';
import * as fromRoot from '../../app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  maxDate;
  isLoading$:Observable<boolean>
  LoadingSub:Subscription
  constructor(private authService:AuthService,private uiServ:UIservice,private store:Store<fromRoot.State>) { }

  ngOnInit(): void {
      this.isLoading$=this.store.select(fromRoot.getIsLoading)
  //  this.LoadingSub= this.uiServ.isLoadingStateChanged.subscribe(isLoadingState=>{
  //     this.isLoading=isLoadingState
  //   })
    this.maxDate=new Date()
    this.maxDate.setFullYear(this.maxDate.getFullYear()-18);
  }
  onSubmit(form:NgForm){
    console.log(form)
    this.authService.onRegister({
      email:form.value.email,
      password:form.value.password
    })
  }
  // ngOnDestroy(){
  //   this.LoadingSub.unsubscribe();
  // }

}
