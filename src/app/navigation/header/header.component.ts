import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Subscription, Observable } from 'rxjs';
import * as fromRoot from '../../app.reducer';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() sideNavToggle=new EventEmitter<void>();
  isAuth$:Observable<boolean>;
  
  constructor(private authService:AuthService,private store:Store<fromRoot.State>) { }

  ngOnInit(): void {
   this.isAuth$=this.store.select(fromRoot.getIsAuthenticated)
  }
  ontoggleSideNav(){
    this.sideNavToggle.emit();
  }
  onLogout(){
    this.authService.logOut()
  }

}
