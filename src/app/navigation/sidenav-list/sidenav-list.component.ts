import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer'

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit{
  @Output() closeSideNav=new EventEmitter<void>();
  isAuth$:Observable<boolean>
  constructor(private authService:AuthService,private store:Store<fromRoot.State>) { }

  ngOnInit(): void {
    this.isAuth$=this.store.select(fromRoot.getIsAuthenticated)
  }
  onLogout(){
    this.closeSideNav.emit();
    this.authService.logOut();
  }
  sideNavClose(){
    this.closeSideNav.emit();
  }

}
