import * as fromUIAction from './ui.action'
import { Action } from '@ngrx/store';

export interface State{
    isLoading:boolean
}
const initState:State={
    isLoading:false
}

export function uiReducer(state=initState,action:Action){
    switch(action.type){
        case fromUIAction.START_LOADING:
            return {
                isLoading:true
            };
        case fromUIAction.STOP_LOADING:
            return {
                isLoading:false
            }
        default:
            return state;
    }
}

export const getIsLoading=(state:State)=>state.isLoading