import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {StopTrainingComponent} from './stop.training'
import { TrainingService } from '../training.service';
import * as fromTraining from '../training.reducer';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-current-trainings',
  templateUrl: './current-trainings.component.html',
  styleUrls: ['./current-trainings.component.css']
})
export class CurrentTrainingsComponent implements OnInit {
  progress=0;
  timer:number;
  constructor(private dialog:MatDialog,private trainingServ:TrainingService,private store:Store<fromTraining.State>) { }

  ngOnInit(): void {
    this.onStartorResume();
  }
  onStartorResume(){
    this.store.select(fromTraining.getActiveTraining).pipe(take(1)).subscribe(exercise=>{
      const step=exercise.duration/100 *1000;
      this.timer=setInterval(()=>{
        this.progress=this.progress+1;
        if(this.progress>=100){
        this.trainingServ.completeExercise();
        clearInterval(this.timer)
        }
      },step)
    })
  }
  onStop(){
    clearInterval(this.timer);
    const dialogRef=this.dialog.open(StopTrainingComponent,{
      data:{
        progress:this.progress
      }
    });
    dialogRef.afterClosed().subscribe(result=>{
      if(result){
        this.trainingServ.cancelExercise(this.progress)
      }
      else{
        this.onStartorResume()
      }
    }

    )
  }

}
