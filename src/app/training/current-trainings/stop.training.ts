import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
selector:'app-stopTraining',
template:`<h1 mat-dialog-title>Are You sure?</h1>
            <mat-dialog-content>
                You already got {{passedData.progress}}%
            </mat-dialog-content>
           <mat-dialog-actions>
                <button mat-button [mat-dialog-close]="true" color="primary">Yes</button>
                <button mat-button [mat-dialog-close]="false" color="primary">No</button>
           </mat-dialog-actions>`
})
export class StopTrainingComponent{
    constructor( @Inject(MAT_DIALOG_DATA) public passedData:any){

    }
}