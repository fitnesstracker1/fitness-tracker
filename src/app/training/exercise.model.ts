export interface Exercise{
    id:String
    name:string
    duration:number
    calories:number
    date?:Date
    state?:'completed' | 'cancelled' | null
}