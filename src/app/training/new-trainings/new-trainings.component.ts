import { Component, OnInit, OnDestroy} from '@angular/core';
import { TrainingService } from '../training.service';
import { NgForm } from '@angular/forms';
import {AngularFirestore} from 'angularfire2/firestore'
import { Subscription, Observable } from 'rxjs';
import { Exercise } from '../exercise.model';
import { UIservice } from 'src/app/shared/ui.service';
import * as fromTraining from '../training.reducer';
import * as fromRoot from '../../app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-new-trainings',
  templateUrl: './new-trainings.component.html',
  styleUrls: ['./new-trainings.component.css']
})
export class NewTrainingsComponent implements OnInit{
  exercises$:Observable<Exercise[]>;
  isLoading$:Observable<boolean>

  constructor(private trainingServ:TrainingService,private db:AngularFirestore,
    private uiServ:UIservice,private store:Store<fromTraining.State>){ }

  ngOnInit(): void {
    this.isLoading$=this.store.select(fromRoot.getIsLoading);
    // this.loadingSub=this.uiServ.isLoadingStateChanged.subscribe(isLoading=>{
    //   this.isLoading=isLoading
    // });
   this.exercises$=this.store.select(fromTraining.getAvailableExercise)
    this.fetchExercises();
  }
  fetchExercises(){
    this.trainingServ.fetchAvailableExercises();
  }
  onStartTraining(form:NgForm){
    this.trainingServ.startExercise(form.value.exercise);
  }
}
