import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { Exercise } from '../exercise.model';
import { TrainingService } from '../training.service';
import { MatSort } from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator'
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromTraining from '../training.reducer';

@Component({
  selector: 'app-past-trainings',
  templateUrl: './past-trainings.component.html',
  styleUrls: ['./past-trainings.component.css']
})
export class PastTrainingsComponent implements OnInit,AfterViewInit{
  displayedColumns=['date','name','duration','calories','state'];
  dataSource= new MatTableDataSource<Exercise>();
  dataSubs:Subscription
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private trainingServ:TrainingService,private store:Store<fromTraining.State>) { }

  ngOnInit(): void {
    this.store.select(fromTraining.getFinishedExercise).subscribe(exercises=>{
      this.dataSource.data=exercises
    });
    this.trainingServ.fetchCompletedExercise();
    this.dataSource.paginator=this.paginator
  }
  ngAfterViewInit(){
    this.dataSource.sort=this.sort;
  }
  doFilter(filterString:String){
    this.dataSource.filter=filterString.trim().toLowerCase()
  }
}
