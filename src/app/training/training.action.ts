import { Action} from '@ngrx/store'
import { Exercise } from './exercise.model';

export const SET_AVAILABLE_TRAINING='[Training] SET_AVAILABLE_TRAINING';
export const SET_FINISHED_TRAINING='[Training] SET_FINISHED_TRAINING';
export const START_TRAINING='[Training] START_TRAINING';
export const STOP_TRAINING='[Training] STOP_TRAINING';

export class SetAvailableTraining implements Action{
   readonly type=SET_AVAILABLE_TRAINING
   constructor(public payload:Exercise[]){}
}

export class SetFinishedTraining implements Action {
    readonly type=SET_FINISHED_TRAINING;
    constructor(public  payload:Exercise[]){}
}

export class StartTraining implements Action {
    readonly type=START_TRAINING;
    constructor(public payload:String){}
}

export class StopTraining implements Action {
    readonly type=STOP_TRAINING;
}
export type TrainingActions= SetAvailableTraining | SetFinishedTraining | StartTraining | StopTraining