import {NgModule} from '@angular/core';
import { NewTrainingsComponent } from './new-trainings/new-trainings.component';
import { CurrentTrainingsComponent } from './current-trainings/current-trainings.component';
import { PastTrainingsComponent } from './past-trainings/past-trainings.component';
import { TrainingComponent } from './training.component';
import { StopTrainingComponent } from './current-trainings/stop.training';
import { SharedModule } from '../shared/shared.module';
import { TrainingRoutingModule} from './training-routing.module';
import { StoreModule } from '@ngrx/store';
import { trainingReducer } from './training.reducer'


@NgModule({
    declarations:[NewTrainingsComponent,
        CurrentTrainingsComponent,
        PastTrainingsComponent,
        TrainingComponent,
        StopTrainingComponent],
    imports:[
        SharedModule,
        TrainingRoutingModule,
        StoreModule.forFeature('training',trainingReducer)
    ],
    exports:[],
    entryComponents:[StopTrainingComponent]
})
export class TrainingModule{}
