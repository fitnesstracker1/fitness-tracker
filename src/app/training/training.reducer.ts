import { Action, createFeatureSelector, createSelector } from '@ngrx/store';
import * as TrainingAction from './training.action';
import { Exercise} from './exercise.model';
import * as fromRoot from '../app.reducer'

export interface TrainingState {
    availableExercises:Exercise[];
    finishedExercises:Exercise[];
    activeTraining:Exercise;
}

export interface State extends fromRoot.State{
    training:TrainingState
}

const initState:TrainingState={
    availableExercises:[],
    finishedExercises:[],
    activeTraining:null
}

export function trainingReducer(state=initState,action:TrainingAction.TrainingActions){
    switch(action.type){
        case TrainingAction.SET_AVAILABLE_TRAINING:
            return {
                ...state,
                availableExercises:action.payload
            };
        case TrainingAction.SET_FINISHED_TRAINING:
            return {
                ...state,
                finishedExercises:action.payload
            }
        case TrainingAction.START_TRAINING:
            return {
                ...state,
                activeTraining:{...state.availableExercises.find(ex=>ex.id==action.payload)}
            }
        case TrainingAction.STOP_TRAINING:
            return {
                ...state,
                activeTraining:null
            }
        default:
            return state;
    }
}

export const getTrainingState=createFeatureSelector<TrainingState>('training');

export const getAvailableExercise=createSelector(getTrainingState,(state:TrainingState)=>state.availableExercises)
export const getFinishedExercise=createSelector(getTrainingState,(state:TrainingState)=>state.finishedExercises)
export const getActiveTraining=createSelector(getTrainingState,(state:TrainingState)=>state.activeTraining)
export const getIsTraining=createSelector(getTrainingState,(state:TrainingState)=>state.activeTraining !== null)