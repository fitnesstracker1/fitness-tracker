import { Exercise } from "./exercise.model";
import { Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { map } from 'rxjs/operators'; //if error in map try this  import   'rxjs/add/operator/map';
import { UIservice } from '../shared/ui.service';
import * as fromTraining from './training.reducer';
import { Store } from '@ngrx/store';
import * as UI_actions from '../shared/ui.action';
import * as TrainingActions from './training.action';
import { take } from 'rxjs/operators';

@Injectable()
export class TrainingService{
    private firebaseSubs:Subscription[]=[];

    constructor(private db:AngularFirestore,private uiServ:UIservice,
        private store:Store<fromTraining.State>){
    }

    fetchAvailableExercises(){
    this.store.dispatch(new UI_actions.StartLoading());
    this.firebaseSubs.push(this.db.collection('availableExercises')
    .snapshotChanges().pipe(
    map(docArray=>{
       return  docArray.map(doc=>{   
          return {
            id:doc.payload.doc.id,
             name:doc.payload.doc.data()['name'], //if no error use doc.payload.doc.data().name
             duration:doc.payload.doc.data()['duration'],
             calories:doc.payload.doc.data()['calories']
            //...doc.payload.doc.data() as Exercise
          } 
        })
    })).subscribe((exercises:Exercise[])=>{
        this.store.dispatch(new UI_actions.StopLoading())
        this.store.dispatch(new TrainingActions.SetAvailableTraining(exercises))
    },error=>{
        this.store.dispatch(new UI_actions.StopLoading())
        this.uiServ.showSnackBar('Fetching Exercises has failed.Please Try Again Later',null,3000);
    }))
    }
    startExercise(selectedId:String){
        // this.runningExercise= this.availableExercises.find(ex=>
        //     ex.id===selectedId)
        // this.exerciseAdded.next({...this.runningExercise})      
        this.store.dispatch(new TrainingActions.StartTraining(selectedId)) 
    }
    completeExercise(){
        this.store.select(fromTraining.getActiveTraining).pipe(take(1)).subscribe(ex=>{
            this.addDataToDataBase({
                ...ex,
                date:new Date(),
                state:'completed'
            });
            this.store.dispatch(new TrainingActions.StopTraining())
        })
    }
    cancelExercise(progress:number){
        this.store.select(fromTraining.getActiveTraining).pipe(take(1)).subscribe(ex=>{
            this.addDataToDataBase({
                ...ex,
                duration:ex.duration*(progress/100),
                calories:ex.calories*(progress/100),
                date:new Date(),
                state:'cancelled'
            });
            this.store.dispatch(new TrainingActions.StopTraining()) 
        })
    }
    cancelFirebaseSubs(){
        this.firebaseSubs.forEach(subs=> subs.unsubscribe())
    }

    fetchCompletedExercise(){
       this.firebaseSubs.push(this.db.collection('completedExercise').valueChanges()
       .subscribe((exercises:Exercise[])=>{
          this.store.dispatch(new TrainingActions.SetFinishedTraining(exercises))
        }))
    }
    addDataToDataBase(exercise:Exercise){
        this.db.collection('completedExercise').add(exercise)
    }
}