// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBj-TKm7FFChtMk4V3bUq7_7e_WuP5Ljvg",
    authDomain: "fitness-tracker-6588c.firebaseapp.com",
    databaseURL: "https://fitness-tracker-6588c.firebaseio.com",
    projectId: "fitness-tracker-6588c",
    storageBucket: "fitness-tracker-6588c.appspot.com",
    messagingSenderId: "909143622422",
    appId: "1:909143622422:web:e86b992354891382bd4637",
    measurementId: "G-PZ5S3VCLWZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
